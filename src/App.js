import React, { Component } from "react";
import "./style.css";
const safeEval = require("safe-eval");

export class App extends Component {
  state = {
    calculation: "",
    result: "",
    lastOperator: "",
  };

  operator = ["/", "*", "+", "-", "."];
  updateCalculation = (value) => {
    const calculationValue = this.state.calculation;

    //Some Base cases
    if ((value === "-" || value === ".") && calculationValue === "") {
      this.setState({
        calculation: value,
        lastOperator: value,
      });
      return;
    }

    if (this.operator.includes(value) && calculationValue === "") {
      return;
    }
    if (
      this.operator.includes(value) &&
      this.operator.includes(calculationValue.slice(-1))
    ) {
      return;
    }
    if (value === ".") {
      if (this.state.lastOperator === ".") {
        return;
      }
    }
    if (this.operator.includes(value)) {
      this.setState({
        lastOperator: value,
      });
    }
    this.setState({
      calculation: calculationValue + value,
    });

    //solve 2+2*
    if (!this.operator.includes(value)) {
      this.setState({
        result: safeEval(calculationValue + value).toString(),
      });
    }
  };

  finalCalculation = () => {
    const finalValue = this.state.result;
    this.setState({
      calculation: finalValue,
      result: "",
    });
    if (finalValue.includes(".")) {
      this.setState({
        lastOperator: ".",
      });
    } else {
      this.setState({
        lastOperator: "",
      });
    }
  };

  deleteFunction = () => {
    const calculationValue = this.state.calculation;
    if (calculationValue === "") {
      return;
    }
    if (calculationValue.slice(-1) === ".") {
      this.setState({
        lastOperator: "",
      });
    }
    this.setState({
      calculation: calculationValue.slice(0, -1),
    });
  };

  insideResultDisplay = () => {
    if (this.state.result === "") {
      return "";
    } else {
      return <span>({this.state.result})</span>;
    }
  };
  insideCalculationDisplay = () => {
    if (this.state.calculation === "") {
      return "0";
    } else {
      return this.state.calculation;
    }
  };

  render() {
    return (
      <div className="App">
        <div className="calculator">
          <div className="display">
            {this.insideResultDisplay()}
            {this.insideCalculationDisplay()}
          </div>

          <div className="operator">
            <button
              onClick={() => {
                this.updateCalculation("/");
              }}
            >
              /
            </button>
            <button
              onClick={() => {
                this.updateCalculation("*");
              }}
            >
              X
            </button>
            <button
              onClick={() => {
                this.updateCalculation("+");
              }}
            >
              +
            </button>
            <button
              onClick={() => {
                this.updateCalculation("-");
              }}
            >
              -
            </button>

            <button
              onClick={() => {
                this.deleteFunction();
              }}
            >
              DEL
            </button>
          </div>

          <div className="digits">
            <button
              onClick={() => {
                this.updateCalculation("1");
              }}
            >
              1
            </button>
            <button
              onClick={() => {
                this.updateCalculation("2");
              }}
            >
              2
            </button>
            <button
              onClick={() => {
                this.updateCalculation("3");
              }}
            >
              3
            </button>
            <button
              onClick={() => {
                this.updateCalculation("4");
              }}
            >
              4
            </button>
            <button
              onClick={() => {
                this.updateCalculation("5");
              }}
            >
              5
            </button>
            <button
              onClick={() => {
                this.updateCalculation("6");
              }}
            >
              6
            </button>
            <button
              onClick={() => {
                this.updateCalculation("7");
              }}
            >
              7
            </button>
            <button
              onClick={() => {
                this.updateCalculation("8");
              }}
            >
              8
            </button>
            <button
              onClick={() => {
                this.updateCalculation("9");
              }}
            >
              9
            </button>
            <button
              onClick={() => {
                this.updateCalculation("0");
              }}
            >
              0
            </button>
            <button
              onClick={() => {
                this.updateCalculation(".");
              }}
            >
              .
            </button>
            <button
              className="equal"
              onClick={() => {
                this.finalCalculation();
              }}
            >
              =
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
