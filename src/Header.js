import React, {Component} from 'react';

class Header extends Component{
  getStyle = ()=>{
    return {
      backgroundColor : '#131a26',
      color: 'white',
      textAlign: 'center',
      height : '60px',
      fontSize: '50px'
    }
  }

  render(){
    return(
      <header style={this.getStyle()}>
        Calculator
      </header>
    )
  }
}
export default Header;